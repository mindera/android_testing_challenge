# Welcome to Android Testing Challenge
![enter image description here](https://image.ibb.co/bXHOB8/rsz_minders_stickers_05.png)

Hello fellow candidate! :wave: 

As a QA Engineer your task is write beautiful automated tests with **Espresso** :coffee: and spot any bug :bug: in the app.

# Task

 - **1st test** - Open Navigation Drawer and verify if the menu is visible as well as the hero image
 - **2nd test** - Verify if the flow action button '+' is displayed to the user, check also the title from the homescreen, finally click add note and verify if the add note UI is visible to the user
 - **3rd test** - This test will add 10 notes with a populated title and description get from [here](https://randomuser.me/api) - title (`first` + `last` name) and description (`street` + `city` + `state` + `postcode`) Verify if the note is saved and displayed to the user in homescreen
 - **4th test** - Open statistics from navigation drawer and verify if the page is opened

## Bonus task

As a talented QAE you are also responsible by integrating the tests in CI so deploy the tests on [CircleCI](https://circleci.com/) and send us the link to the overview page

## Rules and hints

- We believe **Espresso** is the ideal framework to test android native applications, so please just use this framework :-)  
- You are not obliged to solve the **bonus task**, but we believe you are fast learner, so you can do it!
- As a QAE you are not only responsible by developing automated tests, you are also responsible by spotting **bugs** and **UX issues**, so please let us know if you find any!


**Good luck and have fun! :)** 

